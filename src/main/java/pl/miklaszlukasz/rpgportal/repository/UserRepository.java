package pl.miklaszlukasz.rpgportal.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.miklaszlukasz.rpgportal.entity.User;

public interface UserRepository extends JpaRepository<User, Long> {

}
