package pl.miklaszlukasz.rpgportal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RpgportalApplication {

	public static void main(String[] args) {
		SpringApplication.run(RpgportalApplication.class, args);
	}

}
