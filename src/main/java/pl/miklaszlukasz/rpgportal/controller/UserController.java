package pl.miklaszlukasz.rpgportal.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import pl.miklaszlukasz.rpgportal.entity.User;

@Controller
public class UserController {
  @GetMapping("/signup")
  public String showSignUpForm(User user) {
    return "add-user";
  }
}
